# -*- coding: utf-8 -*-
"""
This file contains all the text that is displayed during the instructions and
in between blocks
"""


welcome_message = "Liebe(r) Teilnehmer(in), willkommen zu diesem Experiment! \n\n \
    Drücke 'Leertaste' um fortzufahren"                    

instructions1_wtwait = "In dieser Aufgabe musst du folgendes machen: ...\n\n \
    Während des Experiments, drücke 's' um eine Münze zu verkaufen und 'p', um die verbleibende Zeit bis zum Münzwechsel zu erfahren \n\n \
        Drücke 'Leertaste' um fortzufahren"

ready_training = "Alles verstanden? Drücke 'Leertaste', um mit dem Training zu beginnen"

ready_task = "Du hast das Training erfolgreich beendet. Wenn keine Fragen mehr offen sind, drücke 'Leertaste' um die Aufgabe zu beginnen"

break_task = "Du kannst jetzt eine kurze Pause einlegen. Wenn du den nächsten Block starten möchtest, drücke 'Leertaste'"

finished_wtwaittask = "Der erste Teil ist hiermit beendet. Drücke 'Leertaste' um fortzufahren"

instructions1_timing = "Im zweiten Teil musst du folgendes machen: ... \n\n \
    Du sollst die Zeit schätzen, die zwischen dem ersten Auftreten der ersten Münze bis zum hohen Angebot (zweite Münze) vergangen ist. \n\n \
        Deine Antwort (geschätzte Zeit in Sekunden) kannst du mit den Pfeiltasten angeben."

finished = "Du hast das Experiment erfolgreich beendet. \n\n \
    Vielen Dank für die Teilnahme!"


