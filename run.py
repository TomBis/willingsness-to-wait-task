# -*- coding: utf-8 -*-
"""
This is the run file for the experiment
"""
from psychopy import visual, core, event, gui
from settings import get_settings
settings = get_settings()    

import instruction_texts as i_t
import stimuli


#%% Define helper functions for the experiment
def get_subject_nr():
    ''' Open dialogue box and set subject number
    '''
    dg_box = gui.Dlg(title = 'Experiment')
    dg_box.addField("Probanden-Nr.:")
    dg_box.show()
    subject_nr = int(dg_box.data[0])
    print("\n--\nSubject number: {}".format(subject_nr))   
    return subject_nr

def create_win(full_screen = True): 
    ''' Create experiment window. Choose full_screen = False for debugging
    '''
    if full_screen:
        win = visual.Window(monitor='testMonitor', units='norm', fullscr = True)
    else:
        win = visual.Window([800, 450], monitor='testMonitor', units='norm', fullscr = False)   
    return win

def show_text(win, message):
    ''' Enable showing text, e.g. for providing instructions and wait for button 
    press to continue (if other keys than space should be pressed, change here)
    '''
    text = visual.TextStim(win, message) 
    text.draw()
    win.flip()
    event.waitKeys(keyList=['space'])

        
def run_instructions_wtwait(win):
    ''' Show experiment instructions prior to training
    '''
    show_text(win, i_t.welcome_message) 
    show_text(win, i_t.instructions1_wtwait)    
    show_text(win, i_t.ready_training) 

def run_instructions_timing(win):
    ''' Show experiment instructions prior to training
    '''
    show_text(win, i_t.instructions1_timing)    
    show_text(win, i_t.ready_training) 


#%% Run the experiment

def main():
    # Get subject nr.
    subject_nr = get_subject_nr()
    # Create Win
    win = create_win()
    
    # Show willingsness to wait task instructions
    run_instructions_wtwait(win)
    # Start Training of willingness to wait task
    test = stimuli.Experiment()
    test.run_exp(subject_nr, settings.get('nr_of_blocks_training'), settings.get('block_length_training_wtwait'), 'Training', win, 'wtwait_task')
    # Start Main willingness to wait task
    test.run_exp(subject_nr, settings.get('nr_of_blocks_task'), settings.get('block_length_task_wtwait'), 'Task', win, 'wtwait_task')
    show_text(win, i_t.finished_wtwaittask)

    # Show interval timing task instructions
    run_instructions_timing(win)
    # Start Training of interval timing task
    test.run_exp(subject_nr, settings.get('nr_of_blocks_training'), settings.get('block_length_training_timing'), 'Training', win, 'timing_task')
    # Start Main willingness to wait task
    test.run_exp(subject_nr, settings.get('nr_of_blocks_task'), settings.get('block_length_task_timing'), 'Task', win, 'timing_task')

    # Finish experiment
    show_text(win, i_t.finished)
    win.close()
    core.quit()


if __name__ == "__main__":
    main()