# Willingness-to-wait task

## Run experiment

In order to run the experiment, run the 'run.py' script.

## Change settings

In order to change the settings (e.g. number of blocks, block length) 'settings.py'
needs to be changed.
