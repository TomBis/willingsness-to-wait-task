# -*- coding: utf-8 -*-
"""
This script creates all the stimuli necessary for the experiment including the 
general sequence of the willingness-to-wait task
"""

from psychopy import visual, core, event, gui
from psychopy.visual import ShapeStim 
import numpy as np
import random
import copy

from settings import get_settings
import instruction_texts as i_t
from run import show_text

settings = get_settings()    

import pdb #pdb.set_trace()

#%% Define experiment class

class Experiment():
    '''
    Represents the main willingness-to-wait task
    '''
    def __init__(self):
        pass

#%% Set assignment of experimental conditions
    def get_conditions(self, subject_nr, nr_of_blocks):
        ''' Assign subject to experimental conditions based on the subject nr.
        in order to counterbalance the sequence of environments and corresponding
        token colors across participants. 
        Condition 1 is associated with the uniform distribution
        Condition 2 is associated with the right-skewed distribution 
        (See trial_characteristics for more information)
        The colors are mapped to the block nr., meaning that block1=orange, 
        block2=green, block3=orange, block4=green -> The counterbalance of the
        color-environment mapping stems from the varied condition sequence between
        participants
        '''      
        # Assign experimental condition (=sequence of environments) and token 
        # colors for counterbalance across subjects
        if subject_nr % 2 == 0:
            condition_list = [1,2] * int(nr_of_blocks/2)     
        else:
            condition_list = [2,1] * int(nr_of_blocks/2)
        print("Experimental condition list: {} \n--\n".format(condition_list))        
        return condition_list
    
#%% Set Distribtions for the color change time intervals
    def trial_characteristics(self, condition):
        '''
        Define the specific trial characteristics which vary between trials, 
        namely the amount that needs to be paid in order to check the remaining
        time until the token changes and the actual time until it changes (
        depending on the condition)
        '''
        paid_value = random.randrange(0,11) # Change here for different pay amounts
        distribution_factor = 5000          # Number of cases per 'side' for the skewed distribution
        if condition == 1:
            # Condition 1 represents a uniform distribtion of seconds till token change
            col_change = random.randrange(1,31)
        elif condition == 2:
            # Condition 2 represents a right-skewed distribtion of seconds till token change
            col_change_distribution = np.random.choice(15, distribution_factor, replace=True) + 1  # p=0.5 for t=1-15s
            col_change_distribution = np.append(col_change_distribution, distribution_factor*[60]) # p=0.5 for t=  60s
            col_change = random.choice(col_change_distribution) # pick time interval from distribution here
        print("\nCurrent experimental condition: {}".format(condition))
        print("Current color change interval: {} s".format(col_change))
        return paid_value, col_change

#%% Draw Fixation cross
    def fix_cross(self, win):
        ''' Draw fixation cross between trials
        '''
        fix_cross = visual.TextStim(win=self.win, text="+")
        fix_cross.draw()
        win.flip()
        core.wait(settings.get("iti"))
    
#%% Create Pay-Object
    def waittime_token(self,win, paid_value, pos=(0,0), size=(0.25, 0.44)):
        ''' Create object which represents the amount that needs to be paid
        in order to get information about the remaining time until the token
        changes
        '''
        price_disp = visual.Rect(win=self.win, lineColor=settings.get("outline_col"), 
                                 fillColor=settings.get("payfill_col"), pos=pos, size=size)
        price_text = visual.TextStim(win=self.win, text='{}₵'.format(paid_value), 
                                     color='white', pos=pos, bold=True)
        return price_disp, price_text, paid_value
    
#%% Timing information 

    def progress_bar(self, win, trial_length, col_change=None, flipping=False, remaining_time=False, task_type='wtwait_task'):
        ''' Show progress bar with information about how long the current token
        is visible and how much longer it will take until the token changes
        '''
        timing_disp_remain = None

        # Create outline of box which represents the whole trial time        
        timing_disp_outline = visual.Rect(win=self.win, width=settings.get("progress_box_width"), 
                                          height=settings.get("progress_box_height"), 
                                          lineWidth=1.5, lineColor='black', 
                                          pos=(0.0,settings.get("progress_box_downoffset")), 
                                          fillColor=None)
        
        # Copy this object in order to manipulate filling depending on trial time
        timing_disp_filling = copy.copy(timing_disp_outline)
        timing_disp_filling.fillColor = settings.get('progress_fillcol')
        
        # Filling width is determined by passed trial time
        timing_disp_filling.width = (settings.get('progress_box_width')/settings.get('progress_box_factor')) \
            * ((trial_length/settings.get('trial_length'))*settings.get('progress_box_factor'))
        
        # For accurate drawing of the filling, get the left border of the progress box outline
        left_outline_border = 2 - settings.get("progress_box_width") #  2 in the window width when norm is used
        left_outline_border = -1 + 0.5*left_outline_border           # -1 is left window end -> get distance of progress box from left win end
        
        # Then, get the left starting point of the filling, depending on the width of the filling = trial time
        left_filling_border = timing_disp_filling.width / 2
        left_filling_border = left_outline_border + left_filling_border
        timing_disp_filling.pos = (left_filling_border, settings.get('progress_box_downoffset')) 

        # Draw outline and filling
        timing_disp_outline.draw()
        timing_disp_filling.draw()
        
        # Show remaining time in different color, based on existing filling
        if remaining_time:
            # Calculate width of remaining display
            timing_disp_remain = copy.copy(timing_disp_filling)
            timing_disp_remain.width = col_change/settings.get('trial_length')   # factor of whole box width
            timing_disp_remain.width *= settings.get('progress_box_width')       # remaining display width including already displayed filling
            timing_disp_remain.width -= timing_disp_filling.width                # actual remaining display width
            
            # Calculate left starting point for remaining display
            timing_disp_remain.fillColor = settings.get("progress_remaincol")
            right_filling_border = left_outline_border + timing_disp_filling.width
            left_remaining_border = timing_disp_remain.width/2
            left_remaining_border = right_filling_border + left_remaining_border                        
            timing_disp_remain.pos = (left_remaining_border, settings.get('progress_box_downoffset'))
            timing_disp_remain.draw()
        
        if task_type == 'timing_task':
            # Show 0s and 60s beneath the bar
            zero_s = visual.TextStim(win=self.win, text = '0s', bold=True, 
                                     color=settings.get('progress_fillcol'), 
                                     pos=(left_outline_border,settings.get('progress_box_downoffset')-0.1)) # Position slightly beneath progress box

            sixty_s = visual.TextStim(win=self.win, text = '60s', bold=True, 
                                     color=settings.get('progress_fillcol'), 
                                     pos=(left_outline_border*-1,settings.get('progress_box_downoffset')-0.1))         
            zero_s.draw()
            sixty_s.draw()
        
        if flipping:
            win.flip()
            
        return timing_disp_outline, timing_disp_filling, timing_disp_remain       
  
#%% First token before color change

    def first_token(self, win, total_earnings, total_display, trial_timer, paid_value, block_nr, condition, trial_iteration, task_type): 
        ''' This function is the main function for the display of the first
        token and the other objects which are shown during the time before the
        token color change (i.e. the display of the price for checking the remaining 
                            time, the total amount already gained
                            and the progress bar)
        '''         
        # Show price display for checking remaining time until color change
        if task_type == 'wtwait_task':
            price_disp, price_text, paid_value = self.waittime_token(self.win, paid_value, pos=(-0.75, 0.75), size=(0.15, 0.27))
            price_disp.draw()
            price_text.draw()
        elif task_type == 'timing_task':
            paid_value = None
            
        
        # Set token colors. Right now the first token 
        # color is always orange and the second one green -> only the mapping
        # to the environment is randomized via variable sequence of conditions
        if block_nr % 2 != 0: # Block 1 and 3 will be orange
            token_color = settings.get("circle_col_start1")
            if trial_iteration == 1:
                if condition == 1:
                    print("Current color mapping: {} represents uniform distribution".format(token_color))
                elif condition == 2:
                    print("Current color mapping: {} represents right-skewed distribution".format(token_color))

        elif block_nr % 2 == 0: # Block 2 and 4 will be green
            token_color = settings.get("circle_col_start2")
            if trial_iteration == 1:
                if condition == 1:
                    print("Current color mapping: {} represents uniform distribution".format(token_color))
                elif condition == 2:
                    print("Current color mapping: {} represents right-skewed distribution".format(token_color))
        
        # Create first token including money display
        circle_stim = visual.Circle(win=self.win, radius=settings.get("circle_radius"), 
                                    edges=128, lineWidth=settings.get('circle_outline_thickness'), 
                                    lineColor=settings.get("outline_col"), fillColor=token_color)
        value_text = visual.TextStim(win=self.win, text='{}₵'.format(settings.get("first_token_value")), bold=True)
        circle_stim.draw()
        value_text.draw()
        
        # Show total amount already earned in upper right corner
        if task_type == 'wtwait_task':
            total_earnings = total_earnings
            total_display.draw()
        
        # Show progress bar only during the first 2 blocks
        if task_type == 'wtwait_task' and (block_nr == 1 or block_nr == 2):
            timing_disp_outline, timing_disp_filling, _ = self.progress_bar(self.win,trial_timer)
            timing_disp_outline.draw()
            timing_disp_filling.draw()
            
        # Flip win
        win.flip()
        
        return circle_stim, value_text, paid_value


#%% Second token after the color change

    def changed_token(self, win, total_earnings, total_display, trial_timer, block_nr, task_type):
        ''' This function is the main function for the display of the second
        token and the other objects which are shown during the time after the
        token color change (i.e. the display of the price for checking the remaining 
                            time, the total amount already gained
                            and the progress bar)
        '''
        # Create second token including money display
        circle_stim = visual.Circle(win=self.win, radius=settings.get("circle_radius"), edges=128, lineWidth=settings.get('circle_outline_thickness'), lineColor=settings.get("outline_col"), fillColor=settings.get("circle_col_change"))
        value_text = visual.TextStim(win=self.win, text='{}₵'.format(settings.get("second_token_value")))
        circle_stim.draw()
        value_text.draw()
        
        # Show total amount already earned in upper right corner
        if task_type == 'wtwait_task':
            total_earnings = total_earnings
            total_display.draw()
        
        # Show progress bar only during the first 2 blocks of the wtwait task
        if task_type == 'wtwait_task' and (block_nr == 1 or block_nr == 2):
            timing_disp_outline, timing_disp_filling, _ = self.progress_bar(self.win,trial_timer)
            timing_disp_outline.draw()
            timing_disp_filling.draw()
            
        # Flip win
        win.flip()
        
        return circle_stim, value_text

#%% Sold object 

    def sold_fun(self,win, circle_stim, value_text,total_earnings, total_display): 
        ''' This functions displays 'SOLD' beneath the token after the subject
        sold it (while also showing the total amount'
        '''
        # Show 'SOLD' display
        sold_text = visual.TextStim(win=self.win, text='SOLD', bold=True, color=settings.get("sold_col"), pos=(0.0,-0.5))
        circle_stim.draw()
        value_text.draw()
        sold_text.draw()
        
        # Display total amount already gained
        total_earnings = total_earnings
        total_display.draw()
        win.flip()

#%% Paid object
    def paid_fun(self, win, total_earnings, total_display, trial_timer, paid_value, col_change):
        ''' This function shows the amount-paid-object, the remaining time
        until the token color changes and a corresponding "PAID" display
        '''
        # Show paid object
        price_disp, price_text, paid_value = self.waittime_token(self.win, paid_value, pos=(0,0), size=(0.28, 0.50))
        paid_text = visual.TextStim(win=self.win, text = 'PAID', bold=True, color=settings.get("sold_col"), pos=(0.0,-0.5)) 
        price_disp.draw()
        price_text.draw()
        paid_text.draw()
        
        # Show progres bar with remaining time
        timing_disp_outline, timing_disp_filling, timing_disp_remain = self.progress_bar(self.win, trial_timer, col_change, flipping=False, remaining_time=True)
        timing_disp_outline.draw()
        timing_disp_filling.draw()
        timing_disp_remain.draw()
        
        # Display total
        total_earnings = total_earnings
        total_display.draw()
        
        # Flip
        win.flip()
        
#%%  Gains and losses

    def earnings_fun(self, win, total_earnings, paid_value=0, sold_value=0):
        ''' Calculate gains and losses resulting from paying and selling
        '''
        # Calculate total earnings
        if paid_value == 0:
            total_earnings += sold_value
        elif sold_value == 0:
            total_earnings -= paid_value 
            
        # Show total amount at upper right corner at all times
        total_display = visual.TextStim(win=self.win, text='total: {:.2f} €'.format(total_earnings*0.01), 
                                        bold=True, color=settings.get("total_col"), pos=(0.75, 0.75))
        total_display.draw()
        
        return total_earnings, total_display


#%% Keyboard Input during Willingness to wait task

    def user_input(self, win, token_pressed, circle_stim, value_text, total_earnings, 
                   total_display, paid_value, trial_timer, col_change):
        ''' Get keyboard input from user and enable 'selling' and 'paying' during
        the willingness to wait task
        '''
        # Get only allowed keyboard inputs
        keys = event.getKeys(keyList = ['s', 'p'])
        button_press = False
        
        # Consequences of selling a token
        if 's' in keys:
            self.sold_fun(self.win, circle_stim, value_text, total_earnings, total_display)
            # Different earnings depending on which token is sold
            if token_pressed == 'first':
                sold_value = settings.get("first_token_value")
            elif token_pressed == 'second':
                sold_value = settings.get("second_token_value")
            # Calculate gains by selling current token
            total_earnings, total_display = self.earnings_fun(self.win, total_earnings=total_earnings, sold_value=sold_value)
            core.wait(settings.get("iti"))
            button_press = True
            
        # Consequences of paying for remaing time check
        elif 'p' in keys and token_pressed == 'first':
            # Calculate losses by paying for information about remaining time
            self.paid_fun(self.win, total_earnings, total_display, trial_timer, paid_value, col_change)
            # pdb.set_trace()
            total_earnings, total_display = self.earnings_fun(self.win, total_earnings=total_earnings, paid_value=paid_value)
            core.wait(settings.get("iti"))
            button_press = True
            
        return button_press, total_earnings, total_display 
    
#%% Reporting time helper arrows
    
    def report_arrows(self, win):
        ''' Provide arrow and triangle display as visual helper instruction 
        for interval timing task
        '''
        # Common settings for both arrows and triangle
        line_color = 'red' 
        fill_color = 'red'
        down_offset = settings.get("progress_box_downoffset")-0.15 # How far under the progress box?
               
        # Arrow right
        arrowVert_right = [(0.25,0.05),(0.25,-0.05),(.4,-0.05),(.4,-0.1),(0.6,0),(.4,0.1),(.4,0.05)]
        arrowVert_right = np.array(arrowVert_right)
        arrowVert_right[:,1] += down_offset-0.85 # Move arrow downwards, caution! Not generalized        
        arrow_right = ShapeStim(win=self.win, vertices=arrowVert_right, fillColor=fill_color, size=.5, lineColor=line_color)
        arrow_right.draw()
        
        # Arrow left
        arrowVert_left = copy.copy(arrowVert_right)
        arrowVert_left[:,0] *= -1
        arrow_left = ShapeStim(win=self.win, vertices=arrowVert_left, fillColor=fill_color, size=.5, lineColor=line_color)
        arrow_left.draw()

        # Triangle at the center
        triangle = visual.Polygon(win=self.win, edges=3, radius=.075, 
                                  lineColor=line_color, 
                                  fillColor=fill_color, 
                                  pos=(0, down_offset))
        triangle.draw()
    
#%% Report waiting time during interval timing task

    def report_waittime(self, win):
        ''' Ask the subject to report the waiting time until the token change,
        get the input via the arrow keys and display the action for the subject
        '''
        # Draw instructions for participant
        text = 'REPORT THE WAITING TIME'
        report_text = visual.TextStim(win=self.win, text=text, bold=True, color=settings.get("sold_col"), pos=(0.0,0.0))
        report_text.draw()
        
        # Draw arrow display
        self.report_arrows(self.win)
        
        # Draw inital progress bar before any changes by the user
        self.progress_bar(self.win, settings.get('time_display_start'), flipping=True, task_type='timing_task')
          
        # Give 10s time for answer
        report_timer = core.Clock()
        waittime_estimate = settings.get('time_display_start')
        max_time = settings.get('max_time')
        while report_timer.getTime() < max_time:
            # Get user input and draw the progress bar            
            keys = event.getKeys(keyList = ['left', 'right'])
            if 'left' in keys:
                # Call progress bar with 1s less input time 
                waittime_estimate -= 1
                if waittime_estimate < 0:
                    waittime_estimate = 0
                report_text.draw()
                self.progress_bar(self.win, waittime_estimate, flipping=True, task_type='timing_task')  
            
            elif 'right' in keys:
                # Call progress bar with 1s more input time 
                waittime_estimate += 1
                if waittime_estimate > 60:
                    waittime_estimate = 60
                report_text.draw()
                self.progress_bar(self.win, waittime_estimate, flipping=True, task_type='timing_task')
            
        return waittime_estimate

#%% Put experiment together

    def run_exp(self, subject_nr, nr_of_blocks, block_length, task_or_training, win, task_type):  
        ''' Create the experiment sequence with the desired nr. of blocks and 
        corresponding block length
        '''
        
        # Check whether it's a training or main task sequence
        task_type = task_type
        if task_or_training == 'Task':
            print('\n--\nTASK -- {}'.format(task_type))
        else:
            print('\n--\nTRAINING -- {}'.format(task_type))
        
        # Create Window and condition sequences
        if task_or_training == 'Training': # no extra win is needed after the training
            self.win = win
        condition_list = self.get_conditions(subject_nr, nr_of_blocks)
    
        # Initialise total earnings and corresponding display
        total_earnings = 0 
        total_display = visual.TextStim(win=self.win, text='total: 0.00 €', 
                                        bold=True, color=settings.get("total_col"), 
                                        pos=(0.75, 0.75))
        
        # Loop over desired number of blocks 
        for block_nr in range(1,nr_of_blocks+1):
            print("\n--\nBlock Nr.: {}".format(block_nr))
            if task_or_training == 'Task' and block_nr > 1:
                win.flip()
                win.flip()
                show_text(win, i_t.break_task) 
                
            # Set timer for tracking block length
            block_timer = core.Clock()
            while block_timer.getTime() < block_length: # Block starts here
                # Get specific trial characteristics
                current_condition = condition_list[block_nr-1]
                paid_value, col_change = self.trial_characteristics(current_condition)
                # Draw Fixation Cross
                self.fix_cross(self.win)
                
                # Set timer for tracking trial length
                trial_timer = core.Clock()
                trial_iteration = 0 # helper for not repeating stuff unnecessarily
                while trial_timer.getTime() < settings.get("trial_length")+0.1: # Enable reporting waiting time even in 60s trial
                    # Declare necessary trial characteristics
                    event.clearEvents() # Clear previous keyboard input    
                    trial_iteration += 1
                    
                    # Show all objects which are visible during the appearance of the first token
                    if trial_timer.getTime() < col_change:
                        circle_stim, value_text, paid_value= self.first_token(self.win, total_earnings, total_display, trial_timer.getTime(), paid_value, block_nr, current_condition, trial_iteration, task_type)
                        token_pressed = 'first'
                    
                    # Show all objects which are visible during the appearance of the second token
                    elif trial_timer.getTime() >= col_change:
                        circle_stim, value_text = self.changed_token(self.win, total_earnings, total_display, trial_timer.getTime(), block_nr, task_type)
                        token_pressed = 'second'
                        # If task = interval timing task, not the user input but end of trial determines the following display: after color change, give iti = 1s and then report waiting time
                        if task_type == 'timing_task':
                            core.wait(2.0) # Show high offer for a short time
                            # Call function which shows the report waiting time screen
                            waittime_estimate = self.report_waittime(self.win)
                            print("Actual waiting time: {} s".format(col_change))
                            print("Reported waiting time: {} s".format(waittime_estimate))
                            break    
                        
                    # Get user input and end trial if button is pressed  
                    if task_type == 'wtwait_task':
                        button_press, total_earnings, total_display = self.user_input(self.win, token_pressed, circle_stim, value_text, total_earnings, total_display, paid_value, trial_timer.getTime(), col_change)
                        if button_press:
                            break
                           
                    
        # Specific end of training or end of task instructions
        if task_or_training == 'Training': 
              win.flip()
              show_text(self.win, i_t.ready_task) 
        
        elif task_or_training == 'Task' and task_type == 'wtwait_task' : 
              win.flip()
              show_text(self.win, i_t.finished_wtwaittask)
              print('--\nTotal earned: {} €\n\n'.format(total_earnings*0.01))
          
            