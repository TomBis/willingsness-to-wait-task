# -*- coding: utf-8 -*-
"""
Define experimental settings here
"""

def get_settings():
    '''Return a dictionary of settings.
    '''

    settings = {
    'circle_col_start1': 'orange',          # First token color before color change
    'circle_col_start2': 'limegreen',       # Second token color before color change
    'circle_col_change': 'deepskyblue',     # Token color after color change
    'circle_outline_thickness': 10,         # Thickness of token outline
    'payfill_col': [0.7,0.0,0.0],           # Filling color of price display for remaining-time-check
    'sold_col': 'red',                      # Color of 'SOLD' display 
    'outline_col': 'White',                 # Outline color of token and price object
    'total_col': 'White',                   # Color of total display
    
    'progress_box_height': .10,             # Height of progress box
    "progress_box_width": 1.5,              # Width of progress box
    'progress_box_downoffset': -0.75,       # Offset from center of progress box
    'progress_box_factor': 100,             # Number of parts the progress box is divided into to display passing time
    'progress_fillcol': 'white',            # Color of time progression in box
    'progress_remaincol': 'mistyrose',      # Color of remaining time until color change in progres box
    
    'iti': 2.0,                             # Inter trial interval
    'circle_radius': [0.2,0.35],            # Radius of token
    'first_token_value': 0,                 # Value of first token
    'second_token_value': 30,               # Value of second token
    
    'nr_of_blocks_task': 4,                 # Nr. of blocks during task
    'nr_of_blocks_training': 4,             # Nr. of blocks during training
    
    'block_length_task_wtwait': 60,        # Block length during wtwait task
    'block_length_training_wtwait': 30,    # Block length during wtwait training
    
    'block_length_task_timing': 60,        # Block length during interval timing task
    'block_length_training_timing': 30,    # Block length during interval timing training   
    
    'max_time': 10,                         # Max answering time during interval timing task
    'time_display_start': 30,               # Initial estimate display during interval timing task 
    
    'trial_length': 60,                     # Trial length in task and training in seconds
    }  

    return settings